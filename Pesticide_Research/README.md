Download and install the anaconda software for your OS. Follow default install instructions
LINK: https://docs.anaconda.com/anaconda/install/index.html

After Anaconda is installed:

- Use your PC command prompt to open anaconda, or simply search in your OS for Anaconda Navigator.
- Open the Juypter Notebook or Jupyter Lab environment tab in Anaconda Navigator.
- Open the terminal by clicking the "new" tab on the top right, then git clone the folder from our repository. Type git clone https://gitlab.msu.edu/weinbren/Neogen_Team.git and you will see the repository in your Jupyter files.
- Specifically, you will see a folder called Pesticide_Research, open that folder. 
- Inside the folder, open the file named Research_Draft.ipynb or the PDF, click the tab that says Kernel, then click restart and run all option. All the results of the data, explanation, and research will be presented.

