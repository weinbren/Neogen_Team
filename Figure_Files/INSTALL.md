## Install Anaconda
Install anaconda using this link: https://www.anaconda.com/

On Windows, macOS, and Linux, it is best to install Anaconda for the local user, which does not require administrator permissions and is the most robust type of installation. However, with administrator permissions, you can install Anaconda system wide.

For more details to install on Windows, follow the instructions n: https://docs.anaconda.com/anaconda/install/windows/
On mac, follow the instructions on: https://docs.anaconda.com/anaconda/install/mac-os/

## Open Jupyter Notebook
