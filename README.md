# Neogen_Team
Neogen Pesticide Data Analysis Team 

Members: Tabitha Weinbrenner, Jackson Peacock, Xian Sun, Uta Nishii 

Project Description:

The company sponsoring our team is Neogen, an international food safety company that provides solutions and services for the food processing, animal protein and agriculture industries. The problems we plan to answer throughout the course of our project relate to pesticides used in agriculture and their effects on human health and the environment. Our group intends to perform data collection, cleaning, analysis, modelling, and visualization of our results. We will be using the datasets provided by USDA, from the Pesticide Data Program (PDPD), as well as FDA/FAO data collected for pesticides, public health, and the environment.

The questions we intend to answer through our analysis are as follows: 

- What pesticides are predicted to have the biggest impact on the environment in the next 3-5 years?  

- How do the most harmful pesticides affect the climate overtime? 

- Which food products have pesticide residue levels that exceed FDA and EPA thresholds? 

- Which food products contain multiple pesticides which may or may not exceed regulatory thresholds that pose a greater health risk due to the variety of pesticides with different modes of action?  

- Are there trends in pesticide residue levels on foods over time, by food product, by location, by quantity (tonnage) of pesticides manufactured, by pesticide toxicity or environmental impact, by pesticide class that could be used to predict issues with certain foods?  

- What pesticides will have the biggest impact on human and animal health in the next 3-5 years?  

We hope to finalize the project with a timeseries model and predictive model, and compare our results to scientific articles/research.

Our team will know we’re done when we have successfully answered our top four priority questions, specifically utilizing a time series model and a climate change predictive model, and our results accurately correlate to our research. We will have tested the models statistically and compared them to scientific research, real world data, and other models' predictions to confirm the project accuracy by the time of completion.  

Link to project proposal video: https://youtu.be/ssqvU1mdt1w

In the main branch, we have a folder containing the files needed for reproducing our work, including the instructions to do so. This folder is called  Reproducibility_Files.

Also included in the main branch are the files for all the figures used in the project, these will be included in the folder: Figure_Files.

Our full project is the Pesticide_Report.ipynb file in the base folder of our repo. There is a pdf version included under the same name.
The link to our Final Project video: https://mediaspace.msu.edu/media/Final_Project_Video/1_ur3d7mv0
